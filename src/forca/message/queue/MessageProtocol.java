package forca.message.queue;

public class MessageProtocol {
	 public String handleProtocolMessage(String messageText) {
	        String responseText;
	        if ("MyProtocolMessage".equalsIgnoreCase(messageText)) {
	            responseText = "Saya Terima Pesan Anda : "+messageText;
	        } else {
	            responseText = "Unknown protocol message: " + messageText;
	        }	         
	        return responseText;
	    }
}
